
json.array! @users do |user|
  json.email user.email
  json.id user.id.to_s
  json.name user.name
  json.permitted_projects do
	  json.array! user.project_on_users do |project|
  		json.project_id project.project_id.to_s
  	end
  end
end