json.array! @tutorials do |tutorial|
  json.id tutorial.id.to_s
  json.title tutorial.title
  json.description tutorial.description
  json.implementedOn tutorial.implementedOn
  json.difficulty tutorial.difficulty
end
