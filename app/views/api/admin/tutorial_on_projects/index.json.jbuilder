json.array! @tutorialOnProjects do |tutorialOnProject|
  json.id tutorialOnProject.id.to_s
  json.project_id tutorialOnProject.project_id.to_s
  json.tutorial_id tutorialOnProject.tutorial_id.to_s
  json.tutorial_title tutorialOnProject.tutorial.title
  json.tutorial_description tutorialOnProject.tutorial.description
  json.tutorial_implementedOn tutorialOnProject.tutorial.implementedOn
  json.tutorial_difficulty tutorialOnProject.tutorial.difficulty
end
