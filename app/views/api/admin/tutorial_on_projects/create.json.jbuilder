if @tutorialOnProject.valid?
  json.success true
  json.id @tutorialOnProject.id.to_s
  json.project_id @tutorialOnProject.project_id.to_s
  json.tutorial_id @tutorialOnProject.tutorial_id.to_s
else
  json.success false
end