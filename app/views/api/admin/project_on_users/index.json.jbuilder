json.array! @projects do |project|
	json.id project.project_id.to_s
  json.title project.project.title
  json.description project.project.description
  json.rails project.project.rails
  json.angularJS project.project.angularJS
  json.mongoDB project.project.mongoDB
  json.postgreSQL project.project.postgreSQL
  json.mySQL project.project.mySQL
  json.heroku project.project.heroku
  json.windowsAzure project.project.windowsAzure
  json.eC2 project.project.eC2
end
