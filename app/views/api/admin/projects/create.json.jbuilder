if @project.valid?
  json.success true
  json.id @project.id.to_s
  json.title @project.title
  json.description @project.description
  json.rails @project.rails
  json.angularJS @project.angularJS
  json.mongoDB @project.mongoDB
  json.postgreSQL @project.postgreSQL
  json.mySQL @project.mySQL
  json.heroku @project.heroku
  json.windowsAzure @project.windowsAzure
  json.eC2 @project.eC2
  json.user_id @project.user_id.to_s
else
  json.success false
end