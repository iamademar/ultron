if @image.valid?
  json.success true
  json.id @image.id.to_s
  json.image_url @image.image_file.main.url
  json.tutorial_id @image.tutorial_id.to_s
else
  error = @image.errors.first
  json.success false
  json.error "#{error[0].to_s} #{error[1]}"
end
