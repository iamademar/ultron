( function() {
  'use strict';

  angular.module("admin.ctrl.addTutorialCtrl", [])
  .controller('addTutorialController', [ '$scope', '$location', '$routeParams', 'Project', 'Tutorial', 'TutorialOnProject',
  	function($scope, $location, $routeParams, Project, Tutorial, TutorialOnProject) {
      $scope.project = {};
      $scope.tutorialOnProjects = [];
      $scope.tutorialOnProject = {};
      $scope.max = 5;
      $scope.edit = false;
      
      Project.get({ id: $routeParams.project_id }).$promise.then(function(data) {
       $scope.project = data;
      });

      TutorialOnProject.query({ project_id: $routeParams.project_id }).$promise.then(function(data) {
       $scope.tutorialOnProjects = data;
      });

      $scope.delete = function(tutorialOnProject_id) {
        TutorialOnProject.delete({ id: tutorialOnProject_id }).$promise.then(function(data) {
          TutorialOnProject.query({ project_id: $routeParams.project_id }).$promise.then(function(data) {
            $scope.tutorialOnProjects = data;
          });
        });
      }

      $scope.editForm = function() { 
        $scope.edit = true;
      }

      $scope.cancelEdit = function() { 
        $scope.edit = false;
        Project.get({ id: $routeParams.project_id }).$promise.then(function(data) {
         $scope.project = data;
        });
      }

      $scope.editProject = function() { 
        if(($scope.project.rails || $scope.project.angularJS )&&($scope.project.mongoDB || $scope.project.postgreSQL || $scope.project.mySQL)&&($scope.project.heroku || $scope.project.windowsAzure || $scope.project.eC2)){
          Project.update({ id: $routeParams.project_id}, $scope.project ).$promise.then(function(data) {
            $scope.edit = false;
          });
        }
      }

      $scope.hoveringOver = function(value) {
        $scope.overStar = value;
      };

      $scope.createNewTutorial = function() { 
        if($scope.tutorial.difficulty && $scope.tutorial.title && $scope.tutorial.description && $scope.tutorial.implementedOn){
          var newTutorial = new Tutorial($scope.tutorial);

          newTutorial.$save(newTutorial, function(data) {
            $scope.tutorialOnProject.project_id = $routeParams.project_id;
            $scope.tutorialOnProject.tutorial_id = data.id;
            var newSearchAddTutorial = new TutorialOnProject($scope.tutorialOnProject);
            newSearchAddTutorial.$save(newSearchAddTutorial, function(data) {
            });
            TutorialOnProject.query({ project_id: $routeParams.project_id }).$promise.then(function(data) {
             $scope.tutorialOnProjects = data;
            });
          });
          $('#myModal').modal('toggle');
          $('#myModal').find("input,textarea,select,rating")
                          .val('')
                          .end()
        }
      }
  	}
  ]);
})(); 
