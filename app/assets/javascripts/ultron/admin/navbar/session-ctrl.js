( function() {
  'use strict';

  angular.module('admin.ctrl.sessions', [])
  .controller('sessionController', ['Auth', '$scope', '$location', '$rootScope', 'localStorageService',
    function(Auth, $scope, $location, $rootScope,localStorageService) {
      
      $rootScope.$on('$routeChangeStart', function (event) {
        $scope.viewStep = false;
        $scope.logged_in = localStorageService.get('logged_in'); 
        
        var url = $location.path();
        if ( url.indexOf("/steps/view") > -1) {
          $scope.viewStep = true;
        }
      });

      $scope.$on('devise:unauthorized', function(event, xhr, deferred) {
        $location.path("/");
        alert('You are not allowed to access those pages!');
      });

      $scope.$on('devise:login', function(event, currentUser) {
        $scope.isAuthenticated = true;
      });

      $scope.$on('devise:new-session', function(event, currentUser) {
        $scope.isAuthenticated = true;
      });

      $scope.$on('devise:logout', function(event, oldCurrentUser) {
        $scope.isAuthenticated = false;
      });

      $scope.$on('devise:new-registration', function(event, user) {
        $scope.isAuthenticated = true;
      });

      this.logout = function() {
        Auth.logout().then(function(oldUser) {
          $location.path("/");
          
        }, function(error) {
          // An error occurred logging out.
        });
        localStorageService.clearAll();
        $scope.logged_in = null;
        alert("Successfully logged out!");
      }
    }
  ]);
})();