( function() {
  'use strict';

  angular.module("admin.ctrl.editStepCtrl", ['angularFileUpload'])
  .controller('editStepController', [ '$scope', '$location', '$routeParams', 'Project', 'Tutorial', 'Step', 'FileUploader', 'Image', 'cfpLoadingBar',
    function($scope, $location, $routeParams, Project, Tutorial, Step, FileUploader, Image, cfpLoadingBar) {
      $scope.project = {};
      $scope.tutorial = {};
      $scope.step = {};
      $scope.image = null;
      $scope.images = [];
      $scope.uploading = false;   
      $scope.asd = false;
      

      Project.get({ id: $routeParams.project_id }).$promise.then(function(data) {
       $scope.project = data;
      });

      Tutorial.get({ id: $routeParams.tutorial_id  }).$promise.then(function(data) {
       $scope.tutorial = data;
      });

      Step.get({ id: $routeParams.step_id }).$promise.then(function(data) {
       $scope.step = data;
      });

      Image.query({ tutorial_id: $routeParams.tutorial_id }).$promise.then(function(data) {
       $scope.images = data;
      });

      var tutorial_id = $routeParams.tutorial_id ;
      var uploader = $scope.uploader = new FileUploader({ url: '/api/admin/images/upload_file_image'});
      uploader.onSuccessItem = function(fileItem, response, status, headers) {
        $scope.uploading = false;
        alert("Successfully uploaded image!");
        $scope.image = response.image_url;
        Image.update({ id: response.id, tutorial_id: tutorial_id }).$promise.then(function(data) {
          cfpLoadingBar.complete();
          $scope.asd = false;
          Image.query({ tutorial_id: $routeParams.tutorial_id } ).$promise.then(function(data) {
           $scope.images = data;
          });
        });
      };

      uploader.onProgressItem =  function(fileItem, progress) {
        $scope.uploading = true;
      };

      $scope.imageProfile = function() {
        $scope.asd = true;
        cfpLoadingBar.start();
        uploader.uploadAll();
      }

      $scope.saveEditedStep = function() { 
        Step.update({ id: $routeParams.step_id}, $scope.step ).$promise.then(function(data) {
          $location.path('/project/'+ $routeParams.project_id +'/tutorial/'+$routeParams.tutorial_id+'/step/add');
        });
      }
    }
  ]);
})(); 
