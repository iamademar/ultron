( function() {
  'use strict';

  angular.module("admin.ctrl.editTutorialCtrl", [])
  .controller('editTutorialController', [ '$scope', '$location', '$routeParams', 'Project', 'Tutorial', 'TutorialOnProject', 'Step',
    function($scope, $location, $routeParams, Project, Tutorial, TutorialOnProject, Step) {
      $scope.project = {};
      $scope.tutorial = {};
      $scope.steps = [];
      $scope.max = 5;
      $scope.edit = false;

      Project.get({ id: $routeParams.project_id }).$promise.then(function(data) {
       $scope.project = data;
      });

      Tutorial.get({ id: $routeParams.tutorial_id }).$promise.then(function(data) {
       $scope.tutorial = data;
      });

      Step.query({ tutorial_id: $routeParams.tutorial_id }).$promise.then(function(data) {
       $scope.steps = data;
      });

      $scope.editForm = function() { 
        $scope.edit = true;
      }

      $scope.cancelEdit = function() { 
        $scope.edit = false;
        Tutorial.get({ id: $routeParams.tutorial_id }).$promise.then(function(data) {
         $scope.tutorial = data;
        });
      }

      $scope.editTutorial = function() { 
        Tutorial.update({ id: $routeParams.tutorial_id}, $scope.tutorial ).$promise.then(function(data) {
          $scope.edit = false;
        });
      }

      $scope.delete = function(step_id) {
        Step.delete({ id: step_id }).$promise.then(function(data) {
          Step.query({ tutorial_id: $routeParams.tutorial_id }).$promise.then(function(data) {
            $scope.steps = data;
          });
        });
      }
    }
  ]);
})(); 
