(function() {
  'use strict';

  angular.module('admin.resource.tutorialOnProject', ['ngResource'])
  .factory("TutorialOnProject", [
    '$resource', function($resource) {
      return $resource("/api/admin/tutorial_on_projects/:id.json", {
        id: "@id"
      },{
        update: { method: 'PUT' }
      });
  }]);
})();