(function() {
  'use strict';

  angular.module('admin.resource.tutorial', ['ngResource'])
  .factory("Tutorial", [
    '$resource', function($resource) {
      return $resource("/api/admin/tutorials/:id.json", {
        id: "@id"
      },{
        update: { method: 'PUT' }
      });
  }]);
})();