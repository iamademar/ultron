(function() {
  'use strict';

  angular.module('admin.resource.image', ['ngResource'])
  .factory("Image", [
    '$resource', function($resource) {
      return $resource("/api/admin/images/:id.json", {
        id: "@id"
      },{
        update: { method: 'PUT' }
      });
  }]);
  
})();