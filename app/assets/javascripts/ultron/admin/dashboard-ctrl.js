( function() {
  'use strict';

  angular.module("admin.ctrl.dashboardCtrl", [])
  .controller('dashboardController', ['$scope', 'Project', 'ProjectOnUser',
  	function($scope, Project, ProjectOnUser) {
      $scope.projects = [];

      $scope.$watch('testUser', function(user) {
        if(user != null){
          if(user.user_type == 1){
            Project.query({ user_id: user._id.$oid }).$promise.then(function(data) {
              $scope.projects = data;
              $scope.count = data.length;
            }); 
          }else{
            ProjectOnUser.query({ user_id: user._id.$oid }).$promise.then(function(data) {
              $scope.projects = data;
              $scope.count = data.length;
            }); 
          }
        } 
      }); 
  	}
  ]);
})(); 
