( function() {
  'use strict';

  angular.module('admin.ctrl.signIn', [])
  .controller('signInController', ['Auth', '$scope', '$location', 'localStorageService',
    function(Auth, $scope, $location, localStorageService) {
      this.credentials = { email: '', password: '' };
      $scope.signup = false;

      $scope.$watch('testUser', function(data) {
        if(data != null) {
          $location.path("/dashboard");
        }
      })

      $scope.signIn = function() {
        // Code to use 'angular-devise' component
        Auth.login(this.credentials).then(function(user) {
          localStorageService.set('logged_in', 'true');
          $location.path("/dashboard");
          alert('Successfully signed in user!');
        }, function(error) {
          console.info('Error in authenticating user!');
          alert('Error in signing in user!');
        });
      }
      $scope.login = $scope.signIn;

      $scope.signupForm = function() {
        $scope.signup = true;
      }

      $scope.signing = function() {
        if($scope.credentials.password_confirmation == $scope.credentials.password){
          Auth.register($scope.credentials).then(function(registeredUser) {
            localStorageService.set('logged_in', 'true');
            $location.path("/dashboard");
            alert('Successfully registered!');
          }, function(error) {
          });
        }
      }
      $scope.buttonSignUp = $scope.signing;

    }

  ]);

})();