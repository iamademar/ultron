( function() {
  'use strict';

  angular.module('ultron', [
    // Libraries
    'templates',
    'ngRoute',
    'Devise',
    'ngSanitize',
    'btford.markdown',
    'angular-loading-bar',

    'LocalStorageModule',
    'ui.bootstrap',


    //Resources
    'admin.resource.project',
    'admin.resource.tutorial',
    'admin.resource.tutorialOnProject',
    'admin.resource.step',
    'admin.resource.image',
    'admin.resource.user',
    'admin.resource.projectOnUser',
    'admin.resource.permittedProject',

    //Controllers
    'admin.ctrl.signIn',
    'admin.ctrl.sessions',
    'admin.ctrl.dashboardCtrl',
    'admin.ctrl.createProjectCtrl',
    'admin.ctrl.addTutorialCtrl',
    'admin.ctrl.projectListCtrl',
    'admin.ctrl.searchAddTutorial',
    'admin.ctrl.projectDetailsCtrl',
    'admin.ctrl.addStepsCtrl',
    'admin.ctrl.viewStepCtrl',
    'admin.ctrl.editStepCtrl',
    'admin.ctrl.editTutorialCtrl',
    'admin.ctrl.sideBar',
    'admin.ctrl.admissionCtrl',

  ])

  .config( ['AuthProvider', 'AuthInterceptProvider',
    function(AuthProvider, AuthInterceptProvider) {
      AuthInterceptProvider.interceptAuth(true);
    }
  ])

  .config(['$routeProvider',
    function($routeProvider) {
      $routeProvider
        .when('/', {
          templateUrl: 'ultron/admin/sign-in/sign-in.html'
        })
        .when('/dashboard', {
          templateUrl: 'ultron/admin/dashboard.html'
        })
        .when('/projects', {
          templateUrl: 'ultron/admin/projects-list/projects-list.html'
        })
        .when('/project/new', {
          templateUrl: 'ultron/admin/create-project/create-project.html'
        })
        .when('/project/:project_id/tutorial/add', {
          templateUrl: 'ultron/admin/add-tutorial/add-tutorial.html'
        })
        .when('/project/:project_id/tutorial/new', {
          templateUrl: 'ultron/admin/create-new-tutorial/create-new-tutorial.html'
        })
        .when('/project/:project_id/tutorial/add/search', {
          templateUrl: 'ultron/admin/search-add-tutorial/search-add-tutorial.html'
        })
        .when('/project/:project_id', {
          templateUrl: 'ultron/admin/project-details/project-details.html'
        })
        .when('/project/:project_id/tutorial/:tutorial_id/step/add', {
          templateUrl: 'ultron/admin/add-steps-on-tutorial/add-steps-on-tutorial.html'
        })
        .when('/project/:project_id/tutotial/:tutorial_id/step/add/form', {
          templateUrl: 'ultron/admin/add-steps-on-tutorial/add-steps-form.html'
        })
        .when('/project/:project_id/tutorial/:tutorial_id/steps/view', {
          templateUrl: 'ultron/admin/view-steps/view-steps.html'
        })
        .when('/project/:project_id/tutotial/:tutorial_id/step/:step_id/edit', {
          templateUrl: 'ultron/admin/edit-step/edit-step.html'
        })
        .when('/project/:project_id/tutorial/:tutorial_id/edit', {
          templateUrl: 'ultron/admin/edit-tutorial/edit-tutorial.html'
        })
        .when('/users', {
          templateUrl: 'ultron/admin/admission/admission.html'
        });
    }
  ]);
})();
