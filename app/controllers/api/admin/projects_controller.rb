class API::Admin::ProjectsController < ApplicationController
  before_filter :authenticate_user!

  def create
    @project = Project.create(project_params)
  end

  def show
    @project = Project.where({ id: params[:id] }).first  
  end

  def index
    @projects = Project.where({ user_id: params["user_id"] })
  end

  def update
    @update = Project.where({ id: params["id"] }).first
    @update.update_attributes(project_params)
  end

  private
    def project_params
      params.require(:project).permit(:title, :description, :rails, :angularJS, :mongoDB, :postgreSQL, :mySQL, :heroku, :windowsAzure, :eC2, :user_id)
    end
  
end
