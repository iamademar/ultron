class API::Admin::BannersController < ApplicationController
  skip_before_filter :verify_authenticity_token, only: [:upload_file_image]

  def upload_file_image
      @banner = Banner.create({image_file: params[:file] }) 
      Rails.logger.info params.inspect
  end

  def show
  	@banner = Banner.where({ project_id: params["project_id"] })
 	end

	def update
  	@update = Banner.where({ id: params["id"], }).first
    @update.update_attributes({ project_id: params["project_id"] })
  end


end