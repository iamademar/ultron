class API::Admin::UsersController < ApplicationController
  before_filter :authenticate_user!

  def index
  	if params["config"] == 'false'
    	@users = User.where({ confirmed: false, declined: false })
    elsif params["config"] == 'true'
    	@users = User.where({ confirmed: true, user_type: 0 })
    end
  end

  def update
    @update = User.where({ id: params["id"] }).first
    if params["confirmed"] != nil
    	@update.update_attributes({ confirmed: true })
    elsif params["declined"] != nil
    	@update.update_attributes({ declined: true })
    end
  end
  
end
